#include <iostream>
//global var to recorde the length of input array
int arrayLen;

/*
maxFormLeft: current pose the max value of the left
p : the point to current item
n : array position

it return the min value of the right
*/
int getMinRight(int maxFromLeft,int *p,int n){
    if(n == arrayLen - 1){
        if(maxFromLeft <= *p){
            printf("@ %d",n);
        }
        return *p;
    }
    
    if(*p > maxFromLeft){
        maxFromLeft = *p;
    }
    
    int minFromRight = getMinRight(maxFromLeft, p + 1, n + 1);
    
    if((*p <= minFromRight)&&(*p >= maxFromLeft)){
        printf("@ %d",n);
    }
    
    if(*p < minFromRight){
        minFromRight = *p;
    }
    
    
    return minFromRight;
}


int main(int argc, char** argv)
{
    
    arrayLen = 10;
    int a[] = {4,3,2,3,1,4,7,8,6,9};
    
    
    getMinRight(a[0],a,0);
    
    return 0;
}